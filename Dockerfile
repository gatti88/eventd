FROM golang:1.17-alpine3.15 AS build

WORKDIR /build

RUN echo http://mirror.yandex.ru/mirrors/alpine/v3.12/main > /etc/apk/repositories && \
    echo http://mirror.yandex.ru/mirrors/alpine/v3.12/community >> /etc/apk/repositories && \
    apk update && apk add zip git gcc g++ libc-dev curl jq && \
    download_url=$(curl -s https://api.github.com/repos/go-swagger/go-swagger/releases/latest | \
            jq -r '.assets[] | select(.name | contains("'"$(uname | tr '[:upper:]' '[:lower:]')"'_amd64")) | .browser_download_url') && \
    curl -o /usr/local/bin/swagger -L'#' "$download_url" && \
    chmod +x /usr/local/bin/swagger && \
    go install github.com/golang/mock/mockgen@v1.6.0

ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux
        
COPY go.mod .
COPY go.sum .
RUN  go mod download

COPY . .

RUN go generate ./... && \
    go test ./... && \
    go build -o svc cmd/main.go

FROM alpine:3.15

COPY --from=build /build/svc /svc
ENTRYPOINT ["/svc"]
