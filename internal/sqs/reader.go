package sqs

import (
	"encoding/base64"
	"fmt"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/gatti88/eventd/internal"
)

type readerMsg struct {
	msg *sqs.Message
}

func (x readerMsg) Value() []byte { return []byte(*x.msg.Body) }

func (x readerMsg) Attributes() map[string]string {
	attrs := make(map[string]string)
	attrs["message-id"] = *x.msg.MessageId
	attrs["receipt-handle"] = *x.msg.ReceiptHandle
	attrs["md5-of-body"] = *x.msg.MD5OfBody
	attrs["md5-of-message-attributes"] = *x.msg.MD5OfMessageAttributes

	for k, v := range x.msg.Attributes {
		attrs["message-attributes-"+k] = *v
	}

	for k, v := range x.msg.MessageAttributes {
		if *v.DataType == "Binary" {
			attrs["message-attributes-"+k] = base64.StdEncoding.EncodeToString(v.BinaryValue)
		} else {
			attrs["message-attributes-"+k] = *v.StringValue
		}
	}

	return attrs
}

type reader struct {
	sqs               *sqs.SQS
	queue             string
	readBatch         int
	visibilityTimeout int
	waitTimeSeconds   int
}

// NewReader creates an EventReader instance implemented using Amazon AWS SQS.
// If no credentials are provided the AWS session will be created using implicit IAM permissions.
//
// Input arguments:
//
// @queue: ARN name of the input queue
//
// @readBatch: how many messages get from the queue
//
// @visibilityTimeout: timeout before the message will be re-inserted in the queue
//
// @waitTimeSeconds: wait for listen messages
//
// @region: AWS region
//
// @user: AWS username. This parameter can be empty string.
// In this scenario, the IAM role will be used by default.
//
// @password: AWS password. This parameter can be empty string.
// In this scenario, the IAM role will be used by default.
func NewReader(queue string,
	readBatch int,
	visibilityTimeout int,
	waitTimeSeconds int,
	region string,
	user string,
	password string) internal.EventReader {

	return reader{
		sqs:               createSession(region, user, password),
		queue:             queue,
		readBatch:         readBatch,
		visibilityTimeout: visibilityTimeout,
		waitTimeSeconds:   waitTimeSeconds,
	}
}

func (man reader) Context() (internal.EventContext, error) {
	return internal.EventContextNOOP{}, nil
}

// Read a batch message.
// This method is synchronous and tries to read a batch (configurable with the parameter readBatch)
// from the queue. The result is a list of opaque messages that can be used to commit if necessary.
func (man reader) Read(ctx internal.EventContext) ([]internal.M, error) {
	msg, err := man.sqs.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl:            &man.queue,
		MaxNumberOfMessages: aws.Int64(int64(man.readBatch)),
		VisibilityTimeout:   aws.Int64(int64(man.visibilityTimeout)),
		WaitTimeSeconds:     aws.Int64(int64(man.waitTimeSeconds)),
	})

	result := []internal.M{}

	// No error, retrive messages from SQS response.
	if err == nil {
		for _, v := range msg.Messages {
			result = append(result, readerMsg{msg: v})
		}
	}

	return result, err
}

// Commit removes the message from the queue.
func (man reader) Commit(ctx internal.EventContext, msg []internal.M) error {
	if len(msg) == 0 {
		return nil
	}

	var entries []*sqs.DeleteMessageBatchRequestEntry

	for i, m := range msg {
		entries = append(entries, &sqs.DeleteMessageBatchRequestEntry{
			ReceiptHandle: m.(readerMsg).msg.ReceiptHandle,
			Id:            aws.String(strconv.Itoa(i)),
		})
	}

	resp, err := man.sqs.DeleteMessageBatch(&sqs.DeleteMessageBatchInput{
		QueueUrl: &man.queue,
		Entries:  entries,
	})

	if err != nil {
		return fmt.Errorf("Failed to delete message batch with rec num %d. %v", len(msg), err)
	}

	if len(resp.Failed) > 0 {
		return fmt.Errorf("Failed to delete some messages in batch (%d). First error %v", len(resp.Failed), resp.Failed[0].String())
	}

	return nil
}

func (man reader) Close(ctx internal.EventContext) error {
	return nil
}

func (man reader) Dispose() {}
