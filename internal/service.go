package internal

type ListenService interface {
	Listen()
}

type PublisherService interface {
	ListenAndPublish()
}
