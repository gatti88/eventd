package kafka

import (
	"context"
	"errors"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
	"gitlab.com/gatti88/eventd/internal"
)

type readerMsg struct {
	msg kafka.Message
}

func (x readerMsg) Value() []byte { return x.msg.Value }

func (x readerMsg) Attributes() map[string]string {
	attrs := make(map[string]string)
	attrs["high-water-mark"] = strconv.FormatInt(x.msg.HighWaterMark, 10)
	attrs["key"] = string(x.msg.Key)
	attrs["offset"] = strconv.FormatInt(x.msg.Offset, 10)
	attrs["partition"] = strconv.Itoa(x.msg.Partition)
	attrs["time"] = x.msg.Time.String()
	attrs["topic"] = x.msg.Topic

	for _, v := range x.msg.Headers {
		attrs["message-attributes-"+v.Key] = string(v.Value)
	}

	return attrs
}

// reader structure managed the lifecyle of the kafka underline component.
// with this component you can write read events from a kafka stream.
// This is a generic class, so you can instantiate an infinite number of instances
// from different configurations.
type reader struct {
	reader        *kafka.Reader
	concurrentMax int
}

// NewReader creates and instance of KafkaReader
func NewReader(destination string, topic string, group string, concurrentMax int, maxWaitSeconds int, minBytes int, maxBytes int) internal.EventReader {
	cfg := kafka.ReaderConfig{
		Brokers: []string{destination},

		// Static name of the application. Yes, this parameter could be placed in the
		// configuration file, but for now it can be placed here statically.
		// In the Kafka world, the GroupdID parameter permits to identify a set of different consumers
		// that shares a single pointer to the stream.
		// This means that 2 instances of this service will receive different messages and will be
		// managed as a single application.
		GroupID:  group,
		Topic:    topic,
		MaxWait:  time.Second * time.Duration(maxWaitSeconds),
		MinBytes: minBytes,
		MaxBytes: maxBytes,
	}

	kreader := kafka.NewReader(cfg)

	return reader{
		reader:        kreader,
		concurrentMax: concurrentMax,
	}
}

func (man reader) Context() (internal.EventContext, error) {
	return internal.EventContextNOOP{}, nil
}

func (man reader) Read(ctx internal.EventContext) ([]internal.M, error) {
	var msgs []internal.M

	for i := 0; i < man.concurrentMax; i++ {
		cancelCtx, cn := context.WithTimeout(context.Background(), 1000*time.Millisecond)
		msg, err := man.reader.FetchMessage(cancelCtx)
		cn()

		if err != nil {
			// Timeout expired
			if errors.Is(err, context.DeadlineExceeded) {
				break
			} else {
				return []internal.M{}, err
			}
		} else {
			msgs = append(msgs, readerMsg{msg: msg})
		}
	}

	return msgs, nil
}

// Kafka messages must be deleted manually when the work is done.
// Otherwise they will be re-managed by other consumers.
func (man reader) Commit(ctx internal.EventContext, msg []internal.M) error {
	if len(msg) == 0 {
		return nil
	}

	var entities []kafka.Message

	for _, m := range msg {
		entities = append(entities, m.(readerMsg).msg)
	}

	return man.reader.CommitMessages(context.Background(), entities...)
}

func (man reader) Close(ctx internal.EventContext) error {
	return nil
}

func (man reader) Dispose() {
	man.reader.Close()
}
