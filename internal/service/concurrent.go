package service

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/gatti88/eventd/api"
	"gitlab.com/gatti88/eventd/internal"
)

// asyncState represent the end state of the goroutine used to
// proxy one event to the backend listener
type asyncState struct {
	// msg is the current message
	msg internal.M
	// err is the error occurred in the elaboration.
	// if no error, this must be set to nil value.
	err error

	// critical wil be set to true if the current error is critical and not recoverable
	critical bool
}

// listenServiceConcurrent is the concurrent implementation of the service.
// it will dispatch batch of events using goroutines to parallelize the execution.
type listenServiceConcurrent struct {
	// reader is the source event reader. The implementation-specific logic
	// is defined in the main file.
	reader internal.EventReader

	// criticalErrorNum is the number of current execution CONSECUTIVE critical errors
	// catched. Critical errors are problems that should be addressed killing
	// the agent execution.
	criticalErrorNum int

	// maxCriticalErrorNum is the max number of consecutive critical errors that must
	// to be catched before kill this agent.
	maxCriticalErrorNum int

	// backendUrl is the URL destination of the event proxy action
	backendUrl string

	// log is the shared logger instance
	log *logrus.Entry
}

// NewConcurrent creates a new instance of the service that will dequeue from the event source
// and invoke the backend api.
func NewConcurrent(reader internal.EventReader,
	maxCriticalErrorNum int,
	backendUrl string,
	log *logrus.Entry) internal.ListenService {

	return &listenServiceConcurrent{
		reader:              reader,
		maxCriticalErrorNum: maxCriticalErrorNum,
		criticalErrorNum:    0,
		backendUrl:          backendUrl,
		log:                 log,
	}
}

// Listen for incoming events. This is a blocking operation.
func (svc *listenServiceConcurrent) Listen() {
	svc.log.
		WithField("max-critical-error-num", svc.maxCriticalErrorNum).
		WithField("backend-url", svc.backendUrl).
		Info("Begin listening for events")

	for {
		// Listen outside the endless for.
		err := svc.innerListen()
		if err != nil {

			// critical error found. Increase the counter.
			svc.criticalErrorNum++

			svc.log.
				WithField("critical-error-num", svc.criticalErrorNum).
				Errorf("A critical error occurs: %v", err)

			// Wait for a little before retry
			time.Sleep(10000 * time.Millisecond)

		} else {
			// Reset critical error because we have a success
			svc.criticalErrorNum = 0
		}

		// If critical error panic is enabled, check condition and panic if reached the max number
		if svc.maxCriticalErrorNum > 0 && svc.criticalErrorNum > svc.maxCriticalErrorNum {
			// Terminate the agent with fatal
			svc.log.
				WithField("critical-error-num", svc.criticalErrorNum).
				Fatal("Critial errors max limit reached. Kill this instance!!")
		}
	}
}

// singleMessageExec invokes the backend url for a single message dequeued from the event source.
// It uses the input channel to return the response state to the caller routine.
func (svc *listenServiceConcurrent) singleMessageExec(wg *sync.WaitGroup, asyncErrCh chan asyncState, msg internal.M) {
	hc := &http.Client{
		Timeout: time.Second * 30,
	}

	body := api.BackendDTO{
		Message:    base64.StdEncoding.EncodeToString(msg.Value()),
		Attributes: msg.Attributes(),
	}

	state := asyncState{msg: msg}

	bt, err := json.Marshal(body)
	if err != nil {
		// unmarshal error means that the source event is not in a correct for undestood by this agent.
		state.err = err
		asyncErrCh <- state
		wg.Done()
		return
	}

	// NOTE: errors related to a single message are not critical, basically we skip the message.
	resp, err := hc.Post(svc.backendUrl, "application/json", bytes.NewReader(bt))
	if err != nil {
		state.err = err
	} else if resp.StatusCode != http.StatusOK {
		// Backend returns http code different than 200, so this is an error.
		state.err = fmt.Errorf("Received http error %d from backend", resp.StatusCode)
		// critical because we don't know why the backend fails, so we ca retry the execution next time
		state.critical = true
	}

	asyncErrCh <- state
	wg.Done()
}

// executeAsync will run several goroutines to parallelize the array of input messages.
func (svc *listenServiceConcurrent) executeAsync(msgs []internal.M) []asyncState {
	var wg sync.WaitGroup
	max := len(msgs)
	wg.Add(max)

	asyncErrCh := make(chan asyncState, max)

	for _, m := range msgs {
		go svc.singleMessageExec(&wg, asyncErrCh, m)
	}

	wg.Wait()
	close(asyncErrCh)

	var states []asyncState

	for state := range asyncErrCh {
		states = append(states, state)
	}

	return states
}

// innerListen takes care of the main loop reading messages.
// In this function we retrive the list of available messages and manage the resulting states.
func (svc *listenServiceConcurrent) innerListen() error {
	ctx, err := svc.reader.Context()
	if err != nil {
		return fmt.Errorf("Failed to create reader context: %v", err)
	}

	msgs, err := svc.reader.Read(ctx)
	if err != nil {
		return fmt.Errorf("Failed to read messages from source: %v", err)
	}

	svc.log.WithField("messages-len", len(msgs)).Debug("Got messages from event source, now manage all of them.")
	if len(msgs) == 0 {
		// No messages, so return gracefully and retry another time.
		return nil
	}

	states := svc.executeAsync(msgs)

	var toCommit []internal.M
	for _, state := range states {
		if !state.critical {
			toCommit = append(toCommit, state.msg)
		} else {
			// critical error found, don't delete the message from the queue and retry.
			svc.log.Warnf("Skip commit for message with state %v", state)
		}
	}

	svc.log.WithField("messages-len", len(msgs)).Debug("Commit succeded messages")
	err = svc.reader.Commit(ctx, toCommit)
	if err != nil {
		return err
	}

	return svc.reader.Close(ctx)
}
