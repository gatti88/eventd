package service

import (
	"io/ioutil"
	"net/http"

	"gitlab.com/gatti88/eventd/internal"
)

type restPublisher struct {
	publisher internal.EventPublisher
	port      string
}

func NewPublisher(publisher internal.EventPublisher, port string) internal.PublisherService {
	return restPublisher{
		publisher: publisher,
		port:      port,
	}
}

func (rest restPublisher) ListenAndPublish() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Only post is admitted", 404)
			return
		}

		headerContentType := r.Header.Get("Content-Type")
		if headerContentType != "application/json" {
			http.Error(w, "Content Type is not application/json", http.StatusUnsupportedMediaType)
			return
		}

		if r.Body == nil {
			http.Error(w, "Missing request body for post", 400)
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		ctx, err := rest.publisher.Context()
		if err != nil {
			http.Error(w, "Failed init publisher context", 500)
			return
		}

		result, err := rest.publisher.Publish(ctx, [][]byte{body})
		if err != nil {
			http.Error(w, "Failed to publish messages", 500)
			return
		}

		if result.Status[0] != nil {
			http.Error(w, "Failed to publish single message", 500)
			return
		}

		err = rest.publisher.Commit(ctx, result.ToCommit)
		if err != nil {
			http.Error(w, "Failed to commit messages", 500)
			return
		}
	})

	err := http.ListenAndServe("0.0.0.0:"+rest.port, nil)
	if err != nil {
		panic(err)
	}
}
