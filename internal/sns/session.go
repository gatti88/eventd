package sns

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

func createSession(region string, user string, password string) *sns.SNS {
	var sess *session.Session
	var err error

	// if user and password are configured, use them...
	if user != "" && password != "" {
		sess, err = session.NewSession(&aws.Config{
			Region:      aws.String(region),
			Credentials: credentials.NewSharedCredentials(user, password),
		})
	} else {
		// ...otherwise try to use IAM role.
		sess, err = session.NewSession()
	}

	if err != nil {
		// Critical error, so panic and kill this agent
		panic("Failed to create new AWS session:" + err.Error())
	}

	return sns.New(sess)
}
