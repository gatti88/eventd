package sns

import (
	"encoding/base64"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/google/uuid"
	"gitlab.com/gatti88/eventd/internal"
)

type snsMsg struct {
	messageId      string
	sequenceNumber string
}

func (x snsMsg) Value() []byte                 { return nil }
func (x snsMsg) Attributes() map[string]string { return nil }

type publisher struct {
	sns   *sns.SNS
	topic string
	fifo  bool
	group string
}

// NewPublisher creates an EventPublisher instance implemented using Amazon AWS SNS.
// If no credentials are provided the AWS session will be created using implicit IAM permissions.
//
// Input arguments:
//
// @queue: ARN name of the output topic
//
// @region: AWS region
//
// @user: AWS username. This parameter can be empty string.
// In this scenario, the IAM role will be used by default.
//
// @password: AWS password. This parameter can be empty string.
// In this scenario, the IAM role will be used by default.
func NewPublisher(topic string,
	region string,
	user string,
	password string,
	fifo bool,
	group string) internal.EventPublisher {

	return publisher{
		sns:   createSession(region, user, password),
		topic: topic,
		fifo:  fifo,
		group: group,
	}
}

func (man publisher) Context() (internal.EventContext, error) {
	return internal.EventContextNOOP{}, nil
}

func (man publisher) Publish(ctx internal.EventContext, msgs [][]byte) (internal.PublisherResult, error) {
	output := internal.PublisherResult{
		Status: make(map[int]error),
	}

	for i, msg := range msgs {
		str := base64.StdEncoding.EncodeToString(msg)
		req := sns.PublishInput{
			Message: &str,
		}

		if man.fifo {
			req.MessageDeduplicationId = aws.String(uuid.New().String())
		}

		if man.group != "" {
			req.MessageGroupId = aws.String(man.group)
		}

		req.TopicArn = &man.topic
		resp, err := man.sns.Publish(&req)

		opaqueMsg := snsMsg{
			messageId:      *resp.MessageId,
			sequenceNumber: *resp.SequenceNumber,
		}
		output.Messages = append(output.Messages, opaqueMsg)

		if err != nil {
			output.Status[i] = err
			output.ToCommit = append(output.ToCommit, opaqueMsg)
		}
	}

	return output, nil
}

// Commit removes the message from the queue.
func (man publisher) Commit(ctx internal.EventContext, msg []internal.M) error {
	return nil
}

func (man publisher) Close(ctx internal.EventContext) error {
	return nil
}

func (man publisher) Dispose() {}
