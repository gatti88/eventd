package internal

// EventContext provides an interface to detect if there is an error during the internal event management logic.
type EventContext interface {
	Error(error)
}

// EventContextNOOP is used as empty default context
type EventContextNOOP struct{}

func (e EventContextNOOP) Error(err error) {}

// M is the interface that describes the message that can be published as an event.
// Basically, is a contract that convert a message in a simple byte array.
type M interface {
	// Value return the byte array representation of the message.
	Value() []byte

	// Attributes return a map with the list of attributes linked to this message.
	Attributes() map[string]string
}

// EventReader is the contract that defines the behavior of an event reader.
//
// To support two phase logics (read, elaborate, commit) the interface defines two different methods.
// The commit operation may be different between backend implementations.
// Close method defines a free method for the current read operation, if there are resources to be cleaned.
// The dispose method defines a generic way to release all resources if requested by the underline implementation.
//
// Example:
// 		reader := kafka.NewReader(...)
//		readCtx, err := reader.Context()
// 		if err != nil {
//			panic(err)
//		}
//		opaqueMsg, err := reader.Read(readCtx)
//		if err == nil {
//			msg, err := json.Unmarshal(opaqueMsg.Value())
//			if err == nil {
//				fmt.Printf("msg=%v", msg)
//				_ = reader.Commit(readCtx, opaqueMsg)
//			}
//		}
//		reader.Close(readCtx)
// 		reader.Dispose()
type EventReader interface {
	// Start method is useful when a specific reader needs some operations to be performed before read a set of messages.
	//
	// Example:
	//		reader := kafka.NewReader(...)
	//		readCtx := reader.Context()
	//		msgs, err := reader.Read()
	//
	Context() (EventContext, error)

	// Read method tries to read a new message from the queue. It returns the message in its opaque form and
	// an error (if occurs).
	Read(EventContext) ([]M, error)

	// Commit could be not implemented, it isn't mandatory. Different between implementations.
	//
	// It takes a list of messages in its opaque form an tries to commit it to the message system if the operation
	// is supported.
	Commit(EventContext, []M) error

	// Close terminates any pending operation of the current context.
	Close(EventContext) error

	// Dispose will free every resource allocated by this reader
	Dispose()
}

type PublisherResult struct {
	Messages []M
	Status   map[int]error
	ToCommit []M
}

// EventPublisher represent the interface with the message publish operation.
//
// It provides some usefult methods to publish messages to a destination.
// Example:
// 		publisher := kafka.NewPublisher(...)
//		pubCtx, err := publisher.Context()
//		if err != nil {
//			panic(err)
//		}
//		opaqueMsgs, err := reader.Publish(pubCtx, listOfMessagesBytes)
//		if err == nil {
//			_ = publisher.Commit(pubCtx, opaqueMsgs)
//		}
//		publisher.Close(pubCtx)
// 		publisher.Dispose()
type EventPublisher interface {
	// Start method is useful when a specific reader needs some operations to be performed before read a set of messages.
	//
	// Example:
	//		publisher := kafka.NewPublisher(...)
	//		pubCtx, err := publisher.Context()
	//
	Context() (EventContext, error)

	// Publish takes a list of messages in byte format (for example after a json.Marshal op) and publish all of them to the destination.
	//
	// Note that the return value should be used to commit all messages (it depdends from the undeline implementation)
	Publish(EventContext, [][]byte) (PublisherResult, error)

	// Commit could be not implemented, it isn't mandatory. Different between implementations.
	//
	// It takes a list of messages in its opaque form an tries to commit it to the message system if the operation
	// is supported.
	Commit(EventContext, []M) error

	// Close terminates any pending operation of the current context.
	Close(EventContext) error

	// Dispose will free every resource allocated by this reader
	Dispose()
}
