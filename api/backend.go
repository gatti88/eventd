// Package eventd API.
//
// The purpose of this application is to provide an interface for
// manage async events
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http, https
//     Host: localhost
//     BasePath: /
//     Version: 1.0.0
//     Contact: gatti88
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta

package api

//nolint
var APIVersion string

// swagger:model
type BackendDTO struct {
	Message    string            `json:"message"`
	Attributes map[string]string `json:"attributes"`
}

// swagger:model
type BackendResultDTO struct {
	Message string `json:"message"`
}
