package main

import (
	"context"
	"fmt"
	"os"

	"github.com/mkideal/cli"
	"github.com/sirupsen/logrus"
	"gitlab.com/gatti88/eventd/internal/kafka"
	"gitlab.com/gatti88/eventd/internal/service"
	"gitlab.com/gatti88/eventd/internal/sns"
	"gitlab.com/gatti88/eventd/internal/sqs"
)

func getLogLevel(level string) logrus.Level {
	switch level {
	case "info":
		return logrus.InfoLevel
	case "debug":
		return logrus.DebugLevel
	case "trace":
		return logrus.TraceLevel
	}

	return logrus.InfoLevel
}

type BaseCommand struct {
	cli.Helper

	DestinationUrl      string `cli:"*d,destination-url" usage:"Invoked URL to send received messages"`
	MaxCriticalErrorNum int    `cli:"c,critial-error-count" dft:"10" usage:"Max number of critical errors before kill the agent"`
	MaxConcurrency      int    `cli:"m,concurrency" dft:"10" usage:"Max parallel number of threads to dequeue from source event. This is the number of messages retrived from the source as batch"`
	LogLevel            string `cli:"l,log-level" dft:"info" usage:"Log level for this instance. Admitted values are info,debug,trace"`
	ExecutionType       string `cli:"e,execution-type" dft:"listener" usage:"Execution mode: listener,publisher"`
	Port                string `cli:"po,port" dft:"8080" usage:"In publisher mode, the port which is used to listen for messages to publish"`
}

type sqsCommand struct {
	BaseCommand

	Queue             string `cli:"*q,queue" usage:"AWS SQS queue as source of events"`
	Region            string `cli:"r,region" dft:"" usage:"AWS Region"`
	User              string `cli:"u,user" dft:"" usage:"AWS Username"`
	Password          string `cli:"p,password" dft:"" usage:"AWS Password"`
	VisibilityTimeout int    `cli:"t,visibility-timeout" dft:"60000" usage:"Timeout before the message will be restored in the original source queue in milliseconds"`
	WaitTimeSeconds   int    `cli:"w,wait-time-seconds" dft:"60000" usage:"Time to wait for next get messages operation"`
}

type snsCommand struct {
	BaseCommand

	Topic    string `cli:"*t,topic" usage:"AWS SNS topic destination"`
	Region   string `cli:"r,region" dft:"" usage:"AWS Region"`
	User     string `cli:"u,user" dft:"" usage:"AWS Username"`
	Password string `cli:"p,password" dft:"" usage:"AWS Password"`
	Fifo     bool   `cli:"f,fifo" dft:"false" usage:"Set to true if you want an auto-generated message-uuid"`
	Group    string `cli:"g,group" dft:"" usage:"Mandatory if Fifo is true, same group means message ordering using uuid"`
}

type kafkaCommand struct {
	BaseCommand

	Url         string `cli:"*u,kafka-url" usage:"Kafka URL"`
	Topic       string `cli:"*t,topic" usage:"Kafka topic to subscribe"`
	WaitSeconds int    `cli:"*w,wait-seconds" usage:"Seconds between pollings for new messages"`
	MinBytes    int    `cli:"*m,min-bytes" usage:"Min bytes of messages to download"`
	MaxBytes    int    `cli:"*M,max-bytes" usage:"Max bytes of messages to download"`
	Group       string `cli:"*g,group" usage:"Kafka Group. Every instance of this daemon which uses the same group will receive different messages, otherwise the same message will be delivered to multiple copies"`
}

var rootCmd = &cli.Command{
	Desc: "eventd is a agent that deque events from a source and proxy them to a destination http backend",
	Fn: func(c *cli.Context) error {
		c.String("eventd is a agent that deque events from a source and proxy them to a destination http backend\n")
		return nil
	},
}

var sqsCmd = &cli.Command{
	Name: "sqs",
	Desc: "AWS SQS Configuration",
	Argv: func() interface{} { return new(sqsCommand) },
	Fn: func(c *cli.Context) error {
		cfg := c.Argv().(*sqsCommand)

		if cfg.ExecutionType == "publisher" {
			return fmt.Errorf("Current SQS implementation doesn't support publisher mode, please use SNS")
		}

		log := logrus.New().WithContext(context.Background())
		log.Logger.SetLevel(getLogLevel(cfg.LogLevel))

		sqs := sqs.NewReader(cfg.Queue,
			cfg.MaxConcurrency,
			cfg.VisibilityTimeout,
			cfg.WaitTimeSeconds,
			cfg.Region,
			cfg.User,
			cfg.Password)

		svc := service.NewConcurrent(sqs,
			cfg.MaxCriticalErrorNum,
			cfg.DestinationUrl,
			log,
		)

		defer sqs.Dispose()

		svc.Listen()

		return nil
	},
}

var snsCmd = &cli.Command{
	Name: "sns",
	Desc: "AWS SNS Configuration",
	Argv: func() interface{} { return new(sqsCommand) },
	Fn: func(c *cli.Context) error {
		cfg := c.Argv().(*snsCommand)

		if cfg.ExecutionType == "listener" {
			return fmt.Errorf("Current SNS implementation doesn't support listener mode, please use SQS")
		}

		log := logrus.New().WithContext(context.Background())
		log.Logger.SetLevel(getLogLevel(cfg.LogLevel))

		sns := sns.NewPublisher(cfg.Topic,
			cfg.Region,
			cfg.User,
			cfg.Password,
			cfg.Fifo,
			cfg.Group)

		svc := service.NewPublisher(sns,
			cfg.Port,
		)

		defer sns.Dispose()

		svc.ListenAndPublish()

		return nil
	},
}

var kafkaCmd = &cli.Command{
	Name: "kafka",
	Desc: "Kafka Configuration",
	Argv: func() interface{} { return new(kafkaCommand) },
	Fn: func(c *cli.Context) error {
		cfg := c.Argv().(*kafkaCommand)

		if cfg.ExecutionType == "publisher" {
			return fmt.Errorf("Current Kafka implementation doesn't support publisher mode")
		}

		log := logrus.New().WithContext(context.Background())
		log.Logger.SetLevel(getLogLevel(cfg.LogLevel))

		kafka := kafka.NewReader(cfg.Url,
			cfg.Topic,
			cfg.Group,
			cfg.MaxConcurrency,
			cfg.WaitSeconds,
			cfg.MinBytes,
			cfg.MaxBytes)

		svc := service.NewConcurrent(kafka,
			cfg.MaxCriticalErrorNum,
			cfg.DestinationUrl,
			log)

		defer kafka.Dispose()

		svc.Listen()

		return nil
	},
}

func main() {
	if err := cli.Root(rootCmd,
		cli.Tree(cli.HelpCommand("display help information")),
		cli.Tree(sqsCmd, cli.Tree(cli.HelpCommand("display help information"))),
		cli.Tree(kafkaCmd, cli.Tree(cli.HelpCommand("display help information"))),
	).
		Run(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
